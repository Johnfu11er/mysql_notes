# mysql_notes

### Create docker-compose.yml file
```yaml
version: '3.3'

services:
  db:
    image: mysql
    restart: always
    environment:
      MYSQL_DATABASE: 'db'
      # So you don't have to use root, but you can if you like
      MYSQL_USER: 'user'
      # You can use whatever password you like
      MYSQL_PASSWORD: 'password'
      # Password for root access
      MYSQL_ROOT_PASSWORD: 'password'
    ports:
      # <Port exposed> : <MySQL Port running inside container>
      - '3306:3306'
    expose:
      # Opens port 3306 on the container
      - '3306'
      # Where our data will be persisted
    volumes:
      - my-db:/var/lib/mysql
# Names our volume
volumes:
  my-db:
```
### Start your docker runtime from one of these options
- Rancher:
  - Open the Rancher program and wait for it to finish loading
- Command Line:
  - Start whatever docker service you have installed

### Start your MySql container
- This command starts all resources defined in your docker-compose.yaml file in a detached state which means that it runs in the background:
  ```
  docker-compose up -d
  ```
- Verify that your db container started:
  - Enter the following command in the terminal line:
    ```
    docker ps -a '{{.Names}}'
    ```
    - You should see a list of the running contianers:
      ```
      mysql_notes-db-1  # <---- Your db container will end with 'db-#'
      k8s_metrics-server_ ...  # Containers that start with 'k8s' are used by Rancher
      ...
      ```

### Access the MySql container from the command line
- Login to MySql instance on your database container:
  ```
  docker exec -it <continer_name> mysql -u <user> -p
  ```
- When prompted, enter the database password that was set in the docker-compose.yaml file:
  ```
  Enter password:
  ```
- You should see a mysql prompt:
  ```
  mysql>
  ```
- From here you can list the existing databases:
  ```
  mysql> show databases;
  ```
- You should see:
  ```
  +--------------------+
  | Database           |
  +--------------------+
  | db                 |
  | information_schema |
  | performance_schema |
  +--------------------+
  3 rows in set (0.00 sec)
  ```
- From here you can select the `db` database and do whatever you want:
  ```
  mysql> use db;
  ```

### Access the MySql container from MySQL Workbench
- Download the program from [here](https://dev.mysql.com/downloads/workbench/)
- Install the MySQL Workbench program
- Open MySQL Workbench
- In the menu bar at the top of the screen, click `Database`
- In `Database` drop-down menu, click `Connect to database...`